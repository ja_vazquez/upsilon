

import os, sys, time

lnum = 2, 3, 4, 5, 6

for n in range(0,5):

  num = lnum[n]
  wq_input = """
mode: bycore
N: 30
threads: 6
hostfile: auto
job_name: z0.25_norsd_np0.001_nRT10_r0%i.ini
command: |
     source ~/.bashrc;
     OMP_NUM_THREADS=%%threads%% mpirun -hostfile %%hostfile%% ./cosmomc INI_z0.25_norsd_np0.001_nRT10_r0%i.ini > chains/Sim/logs/INI_z0.25_norsd_np0.001_nRT10_r0%i.log 2>chains/Sim/logs/INI_z0.25_norsd_np0.001_nRT10_r0%i.err
  """%(num, num, num, num)

  with open('wq_z0.25_norsd_np0.001_nRT10_r0%i.ini'%(num), 'w') as f:
       f.write(wq_input)

