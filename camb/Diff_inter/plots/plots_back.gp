#set term post eps enhanced "Times Roman" 25



set style line 1 lt 1 lw 3 pt 3 lc rgb "#0000FF"
set style line 2 lt 1 lw 3 pt 3 lc rgb "#33CCFF"
set style line 3 lt 1 lw 3 pt 3 lc rgb "#FF0066"

set term post eps enhanced color "Times Roman" 25
set output "Hubble.eps"

set key left 
set xlabel "{redshift, z}" font "Times Roman, 25"
set title "Hubble Parameter" font "Times Roman, 25" 
set ylabel "{H(z) [Km s^{-1}Mpc^{-1}]}" font "Times Roman, 25"

plot [:][:]'Hubble.dat' u 1:2 notitle w l ls 1


set term post eps enhanced color "Times Roman" 25
set output "Accel.eps"

set title "Deceleration Parameter" font "Times Roman, 25"
set xlabel "{redshift, z}" font "Times Roman, 25"
set ylabel "{q(z)}" font "Times Roman, 25"

plot [:][:]'Hubble.dat' u 1:3  notitle w l ls 1, 0 notitle


set term post eps enhanced color "Times Roman" 21
set output "Modulus.eps"

set title "Distance modulus"   font "Times Roman, 25"
set xlabel "{redshift, z}"  font "Times Roman, 25"
set ylabel "{{/Symbol m}}"  font "Times Roman, 25"

plot [:1.5][34:]'Modulus1.dat' u 1:4  title '{/Symbol W}_{m}=0.25, {/Symbol W}_{/Symbol L}=0.75' w l ls 1,'Modulus2.dat' u 1:4  title '{/Symbol W}_{m} =0.25,   {/Symbol W}_{/Symbol L} = 0' w l ls 2,'Modulus3.dat' u 1:4  title '{/Symbol W}_{m} = 1,    {/Symbol W}_{/Symbol L} = 0' w l ls 3
