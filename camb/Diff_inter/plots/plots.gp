#set term post eps enhanced "Times Roman" 25



set style line 1 lt 1 lw 3 pt 3 lc rgb "#B0171F"
set style line 2 lt 1 lw 3 pt 3 lc rgb "#FFAEB9"
set style line 3 lt 1 lw 3 pt 3 lc rgb "#104E8B"
set style line 4 lt 1 lw 3 pt 3 lc rgb "#63B8FF"
set style line 6 lt 1 lw 1 pt 3 lc rgb "#000000"

set term post eps enhanced color "Times Roman" 25
set output "DA_rs.eps"

set key bottom 
set xlabel "{redshift, z}" font "Times Roman, 25"
#set title "Hubble Parameter" font "Times Roman, 25" 
set ylabel "{[D_A/r_s] / [D_A/r_s]_{Planck} }" font "Times Roman, 25"

plot [:2.5][0.8:1.2] 'ratio_w07_w1.dat' u 1:2 title 'w=-0.7' w l ls 1, 'ratio_w14_w1.dat' u 1:2 title 'w=-1.4'  w l ls 2, 'ratio_op05_w1.dat' u 1:2 title '{/Symbol W}_{k}=0.005' w l ls 3, 'ratio_on05_w1.dat' u 1:2 title '{/Symbol W}_{k}=-0.005' w l ls 4


set term post eps enhanced color "Times Roman" 25
set output "DV_rs.eps"

#set title "Deceleration Parameter" font "Times Roman, 25"
set xlabel "{redshift, z}" font "Times Roman, 25"
set ylabel "{[D_V/r_s] / [D_V/r_s]_{Planck} }" font "Times Roman, 25"

plot [:2.5][0.8:1.2] 'ratio_w07_w1.dat' u 1:3 title 'w=-0.7' w l ls 1, 'ratio_w14_w1.dat' u 1:3 title 'w=-1.4'  w l ls 2, 'ratio_op05_w1.dat' u 1:3 title '{/Symbol W}_{k}=0.005' w l ls 3, 'ratio_on05_w1.dat' u 1:3 title '{/Symbol W}_{k}=-0.005' w l ls 4


set term post eps enhanced color "Times Roman" 25
set output "H_rs.eps"

#set title "Deceleration Parameter" font "Times Roman, 25"
set xlabel "{redshift, z}" font "Times Roman, 25"
set ylabel "{[Hr_s]_{Planck} / [Hr_s] }" font "Times Roman, 25"

plot [:2.5][0.8:1.2] 'ratio_w07_w1.dat' u 1:4 title 'w=-0.7' w l ls 1, 'ratio_w14_w1.dat' u 1:4 title 'w=-1.4'  w l ls 2, 'ratio_op05_w1.dat' u 1:4 title '{/Symbol W}_{k}=0.005' w l ls 3, 'ratio_on05_w1.dat' u 1:4 title '{/Symbol W}_{k}=-0.005' w l ls 4



#set term post eps enhanced color "Times Roman" 21
#set output "Modulus.eps"

#set title "Distance modulus"   font "Times Roman, 25"
#set xlabel "{redshift, z}"  font "Times Roman, 25"
#set ylabel "{{/Symbol m}}"  font "Times Roman, 25"

#plot [:1.5][34:]'Modulus1.dat' u 1:4  title '{/Symbol W}_{m}=0.25, {/Symbol W}_{/Symbol L}=0.75' w l ls 1,'Modulus2.dat' u 1:4  title '{/Symbol W}_{m} =0.25,   {/Symbol W}_{/Symbol L} = 0' w l ls 2,'Modulus3.dat' u 1:4  title '{/Symbol W}_{m} = 1,    {/Symbol W}_{/Symbol L} = 0' w l ls 3
