#set term post eps enhanced "Times Roman" 25

set style line 1 lt 1 lw 3 pt 3 lc rgb "#B0171F"
set style line 2 lt 1 lw 3 pt 3 lc rgb "#FFAEB9"
set style line 3 lt 1 lw 3 pt 3 lc rgb "#104E8B"
set style line 4 lt 1 lw 3 pt 3 lc rgb "#63B8FF"
set style line 6 lt 1 lw 1 pt 3 lc rgb "#000000"

set term post eps enhanced color "Times Roman" 25
set output "DA_rs.eps"

set key bottom 
set xlabel "{redshift, z}" font "Times Roman, 25"
set title "w=const" font "Times Roman, 25" 
set ylabel "{[D_A/r_s] / [D_A/r_s]_{Planck} }" font "Times Roman, 25"

plot [:2.5][0.8:1.2] 'ratio_w06.dat' u 1:2 title 'w=-0.6' w l ls 1, 'ratio_w08.dat' u 1:2 title 'w=-0.8'  w l ls 2, 1 title '{/Symbol L}' ls 5, 'ratio_w12.dat' u 1:2 title 'w=-1.2' w l ls 3, 'ratio_w14.dat' u 1:2 title 'w=-1.4' w l ls 4



set term post eps enhanced color "Times Roman" 25
set output "H_rs.eps"

set title "w=const" font "Times Roman, 25"
#set title "Deceleration Parameter" font "Times Roman, 25"
set xlabel "{redshift, z}" font "Times Roman, 25"
set ylabel "{[Hr_s]_{Planck} / [Hr_s] }" font "Times Roman, 25"

plot [:2.5][0.8:1.2] 'ratio_w06.dat' u 1:3 title 'w=-0.6' w l ls 1, 'ratio_w08.dat' u 1:3 title 'w=-0.8'  w l ls 2, 1 title '{/Symbol L}' ls 5, 'ratio_w12.dat' u 1:3 title 'w=-1.2' w l ls 3, 'ratio_w14.dat' u 1:3 title 'w=-1.4' w l ls 4



set term post eps enhanced color "Times Roman" 25
set output "Gal_Lya.eps"

set title "w=const" font "Times Roman, 25"
#set title "Distance modulus"   font "Times Roman, 25"
set xlabel "{[D_A/r_s] / [D_A/r_s]_{Planck} }"  font "Times Roman, 25"
set ylabel "{[Hr_s]_{Planck} / [Hr_s] }"  font "Times Roman, 25"
set label "w=-0.6" at 1.0, 0.967
set label "w=-1.4" at 1.0, 1.015
set label "w=-0.6" at 1.06, 1.015
set label "w=-1.4" at 0.94, 1.01


plot [:][:]'Galaxies.dat' u 2:3  title 'Galaxies'  ls 1, 'Galaxies.dat' u 2:3 notitle w l ls 1, 'Lya.dat' u 2:3  title 'Ly{/Symbol a}' ls 2, 'Lya.dat' u 2:3 notitle w l ls 2,  'Const.dat' title '{/Symbol L}' ls 3
