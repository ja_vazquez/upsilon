#set term post eps enhanced "Times Roman" 25

set style line 1 lt 1 lw 3 pt 3 lc rgb "#B0171F"
set style line 2 lt 1 lw 3 pt 3 lc rgb "#FFAEB9"
set style line 3 lt 1 lw 3 pt 3 lc rgb "#104E8B"
set style line 4 lt 1 lw 3 pt 3 lc rgb "#63B8FF"
set style line 6 lt 1 lw 1 pt 3 lc rgb "#000000"

set term post eps enhanced color "Times Roman" 25
set output "DA_rs_wa.eps"

set key bottom 
set xlabel "{redshift, z}" font "Times Roman, 25"
set title "{w_0=-1.0}" font "Times Roman, 25" 
set ylabel "{[D_A/r_s] / [D_A/r_s]_{Planck} }" font "Times Roman, 25"

plot [:2.5][0.8:1.2] 'ratio_wneg1.dat' u 1:2 title '{w_a=-1.0}' w l ls 1, 'ratio_wneg05.dat' u 1:2 title '{w_a=-0.5}'  w l ls 2, 1 title '{/Symbol L}' ls 5, 'ratio_wpos05.dat' u 1:2 title '{w_a=0.5}' w l ls 3, 'ratio_wpos1.dat' u 1:2 title '{w_a=1.0}' w l ls 4



set term post eps enhanced color "Times Roman" 25
set output "H_rs_wa.eps"

set title "{w_0=-1.0}" font "Times Roman, 25"
#set title "Deceleration Parameter" font "Times Roman, 25"
set xlabel "{redshift, z}" font "Times Roman, 25"
set ylabel "{[Hr_s]_{Planck} / [Hr_s] }" font "Times Roman, 25"

plot [:2.5][0.8:1.2] 'ratio_wneg1.dat' u 1:3 title '{w_a=-1.0}' w l ls 1, 'ratio_wneg05.dat' u 1:3 title '{w_a=-0.5}'  w l ls 2, 1 title '{/Symbol L}' ls 5, 'ratio_wpos05.dat' u 1:3 title '{w_a=0.5}' w l ls 3, 'ratio_wpos1.dat' u 1:3 title '{w_a=1.0}' w l ls 4



set term post eps enhanced color "Times Roman" 25
set output "Gal_Lya_wa.eps"

set title "{w_0=-1.0}" font "Times Roman, 25"
##set title "Distance modulus"   font "Times Roman, 25"
set xlabel "{[D_A/r_s] / [D_A/r_s]_{Planck} }"  font "Times Roman, 25"
set ylabel "{[Hr_s]_{Planck} / [Hr_s] }"  font "Times Roman, 25"
set label "{w_a=-1.0}" at 0.94, 0.985
set label "{w_a=1.0}" at 1.09, 1.065
set label "{w_a=1.0}" at 1.02, 0.985
set label "{w_a=-1.0}" at 0.99, 1.025


plot [:][:]'Galaxies.dat' u 2:3  title 'Galaxies'  ls 1, 'Galaxies.dat' u 2:3 notitle w l ls 1, 'Lya.dat' u 2:3  title 'Ly{/Symbol a}' ls 2, 'Lya.dat' u 2:3 notitle w l ls 2,  'Const.dat' title '{/Symbol L}' ls 3
