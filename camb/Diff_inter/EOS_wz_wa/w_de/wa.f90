! describe the dark  energy equation
!

  module Precision
      implicit none

      integer, parameter :: dl = KIND(1.d0)
      integer, parameter :: sp = KIND(1.0)

  end module Precision

      program nada
        use precision
         integer :: i
         integer, parameter :: Nmax =1500
         real, parameter    :: d=2.5/Nmax
         real(dl) z, w_de

         real, parameter    :: w0=-1.0, wa =1.0 
   
 do i=0,Nmax
     z=0.0+d*i 
     w_de=w0+wa*z/(1.0+z) 

     print *,  z, w_de
 enddo


end program nada
