#set term post eps enhanced "Times Roman" 25

set style line 1 lt 1 lw 3 pt 3 lc rgb "#B0171F"
set style line 2 lt 1 lw 3 pt 3 lc rgb "#FFAEB9"
set style line 3 lt 1 lw 3 pt 3 lc rgb "#104E8B"
set style line 4 lt 1 lw 3 pt 3 lc rgb "#63B8FF"
set style line 6 lt 1 lw 1 pt 3 lc rgb "#000000"

set term post eps enhanced color "Times Roman" 25
set output "DA_rs_inter.eps"

set key top right
set xlabel "{redshift, z}" font "Times Roman, 25"
set title "w_{cut off}" font "Times Roman, 25" 
set ylabel "{[D_A/r_s] / [D_A/r_s]_{Planck} }" font "Times Roman, 25"

plot [:2.5][0.95:] 'ratio_inter05.dat' u 1:2 title '{z_{cut}=0.5}' w l ls 1, 'ratio_inter1.dat' u 1:2 title '{z_{cut}=1.0}'  w l ls 2, 'ratio_inter15.dat' u 1:2 title '{z_{cut}=1.5}' w l ls 3, 'ratio_inter2.dat' u 1:2 title '{z_{cut}=2.0}' w l ls 4, 1 title '{/Symbol L}' ls 5



set term post eps enhanced color "Times Roman" 25
set output "H_rs_inter.eps"

set title "w_{cut off}" font "Times Roman, 25"
#set title "Deceleration Parameter" font "Times Roman, 25"
set xlabel "{redshift, z}" font "Times Roman, 25"
set ylabel "{[Hr_s]_{Planck} / [Hr_s] }" font "Times Roman, 25"

plot [:2.5][0.95:] 'ratio_inter05.dat' u 1:3 title '{z_{cut}=0.5}' w l ls 1, 'ratio_inter1.dat' u 1:3 title '{z_{cut}=1.0}'  w l ls 2, 'ratio_inter15.dat' u 1:3 title '{z_{cut}=1.5}' w l ls 3, 'ratio_inter2.dat' u 1:3 title '{z_{cut}=2.0}' w l ls 4, 1 title '{/Symbol L}' ls 5



set term post eps enhanced color "Times Roman" 25
set output "Gal_Lya_inter.eps"
set key bottom right

set title "w_{cut off}" font "Times Roman, 25"
##set title "Distance modulus"   font "Times Roman, 25"
set xlabel "{[D_A/r_s] / [D_A/r_s]_{Planck} }"  font "Times Roman, 25"
set ylabel "{[Hr_s]_{Planck} / [Hr_s] }"  font "Times Roman, 25"
set label "z_{cut}=0.5" at 1.25, 1.235
set label "z={cut}=0.5" at 1.15, 1.025
#set label "w=-0.6" at 1.06, 1.015
#set label "w=-1.4" at 0.94, 1.01


plot [:][:]'Galaxies.dat' u 2:3  title 'Galaxies'  ls 1, 'Galaxies.dat' u 2:3 notitle w l ls 1, 'Lya.dat' u 2:3  title 'Ly{/Symbol a}' ls 2, 'Lya.dat' u 2:3 notitle w l ls 2,  'Const.dat' title '{/Symbol L}' ls 3
