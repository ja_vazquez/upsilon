#set term post eps enhanced "Times Roman" 25



set style line 1 lt 1 lw 3 pt 3 lc rgb "#B0171F"
set style line 2 lt 1 lw 3 pt 3 lc rgb "#FFAEB9"
set style line 3 lt 1 lw 3 pt 3 lc rgb "#104E8B"
set style line 4 lt 1 lw 3 pt 3 lc rgb "#63B8FF"
set style line 6 lt 1 lw 1 pt 3 lc rgb "#000000"

set term post eps enhanced color "Times Roman" 25
set output "w_inter.eps"

set key bottom 
set xlabel "{redshift, z}" font "Times Roman, 25"
set title "{w_{de}, z_{cut}}" font "Times Roman, 25" 
set ylabel "{w_{de} }" font "Times Roman, 25"

plot [:2.5][-1.1:0.1] 'w_z05.dat' u 1:2 title '{z_{cut}=0.5}' w l ls 1,  'w_z15.dat' u 1:2 title '{z_{cut}=1.5}' w l ls 3,  -1 title '{/Symbol L}' ls 5


#'ratio_wa_neg1.dat' u 1:2 title '{w_a=-1.0}' w l ls 1, 'ratio_wa_neg05.dat' u 1:2 title '{w_a=-0.5}'  w l ls 2, 1 title 'w=-1.0' ls 5, 'ratio_wa_pos05.dat' u 1:2 title '{w_a=0.5}' w l ls 3, 'ratio_wa_pos1.dat' u 1:2 title '{w_a=1.0}' w l ls 4


#set term post eps enhanced color "Times Roman" 25
#set output "Dv_rs.eps"

#set title "Deceleration Parameter" font "Times Roman, 25"
#set xlabel "{redshift, z}" font "Times Roman, 25"
#set ylabel "{[D_V/r_s] / [D_V/r_s]_{Planck} }" font "Times Roman, 25"

#plot [:2.5][0.8:1.2] 'ratio_w07_w1.dat' u 1:3 title 'w=-0.7' w l ls 1, 'ratio_w14_w1.dat' u 1:3 title 'w=-1.4'  w l ls 2, 'ratio_op05_w1.dat' u 1:3 title '{/Symbol W}_{k}=0.005' w l ls 3, 'ratio_on05_w1.dat' u 1:3 title '{/Symbol W}_{k}=-0.005' w l ls 4


#set term post eps enhanced color "Times Roman" 25
#set title "{w_0=-1.0}" font "Times Roman, 25"
#set output "H_rs_wa.eps"

#set title "Deceleration Parameter" font "Times Roman, 25"
#set xlabel "{redshift, z}" font "Times Roman, 25"
#set ylabel "{[Hr_s]_{Planck} / [Hr_s] }" font "Times Roman, 25"

#plot [:2.5][0.8:1.2] 'ratio_wa_neg1.dat' u 1:3 title '{w_a=-1.0}' w l ls 1, 'ratio_wa_neg05.dat' u 1:3 title '{w_a=-0.5}'  w l ls 2, 1 title 'w=-1.0' ls 5, 'ratio_wa_pos05.dat' u 1:3 title '{w_a=0.5}' w l ls 3, 'ratio_wa_pos1.dat' u 1:3 title '{w_a=1.0}' w l ls 4



#set term post eps enhanced color "Times Roman" 25
#set output "Gal_Lya.eps"

#set title "Distance modulus"   font "Times Roman, 25"
#set xlabel "{[D_A/r_s] / [D_A/r_s]_{Planck} }"  font "Times Roman, 25"
#set ylabel "{[Hr_s]_{Planck} / [Hr_s] }"  font "Times Roman, 25"
#set label "w=-0.6" at 1.08,1.06
#set label "w=-1.4" at 0.93, 0.97

#plot [:][:]'Galaxies.dat' u 2:3  title 'Galaxies'  ls 1,'Lya.dat' u 2:3  title 'Ly{/Symbol a}' ls 2, 'Const.dat' notitle ls 3
