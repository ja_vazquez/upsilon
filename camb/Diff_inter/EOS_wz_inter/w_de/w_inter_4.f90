
!Change the  Cubic spline and  use
! linear interpolation to  
! to describe the dark  energy equation
! of  state w(z)

   module Precision
      implicit none

      integer, parameter :: dl = KIND(1.d0)
      integer, parameter :: sp = KIND(1.0)

   end module Precision
   

       module LambdaGeneral
         use  Precision
         implicit none


         real(dl)  :: w_lam = -1 !p/rho for the dark energy (assumed constant) 
         real(dl) :: cs2_lam = 1_dl

         logical :: w_perturb = .true.

         logical usew0wa
         real(dl) w0_ppf, wa_ppf
         integer, parameter :: nwmax = 4, nde = 200 !!5000 !2000
         integer :: nw_ppf
         real(dl) w_ppf(nwmax), a_ppf(nwmax), ddw_ppf(nwmax), z_ppf(nwmax)
         real(dl) rde(nde),ade(nde),ddrde(nde)
         real(dl), parameter :: amin = 1.d-9
         private nde,ddw_ppf,rde,ade,ddrde,amin
         contains


!linear in z
         function w_de(a)
          implicit none
         real(dl) :: w_de, al, zl
         real(dl), intent(IN) :: a
          integer i

            al=dlog(a)
            zl=(1.0-a)/a

            if(zl.gt.z_ppf(1)) then
                w_de=w_ppf(1)                   
            elseif(zl.lt.z_ppf(nw_ppf)) then
                w_de=w_ppf(nw_ppf)              
            else
                do i=1,nw_ppf-1
                      z_ppf(i)=(1.0-a_ppf(i))/a_ppf(i)
                      if(zl>=z_ppf(i+1) .and. zl<=z_ppf(i) ) &
                      w_de= (w_ppf(i)*(z_ppf(i+1)-zl)+w_ppf(i+1)*(zl-z_ppf(i)))/(z_ppf(i+1)-z_ppf(i))  	
                 enddo  
           endif
         end function w_de  ! equation of state of the PPF DE 



       end module LambdaGeneral




 program nada

 use precision
 use LambdaGeneral
 integer :: j
 integer,  parameter	 :: np= 19, n=2390
	real(dl) al, za
	real(dl)	:: Params(np)
	real(dl)	:: wa_0(1:n), wa_1(1:n), wa_3(1:n) ,wa_2(1:n), Li(1:n)
        character(LEN=*), parameter :: cl_infile = 'test_act_w4post_equal_weights.dat'

        real(dl), parameter:: zcut=2

!open(unit=14,file=Cl_infile,form='formatted',status='old')

!do j=1,n
! read (14,*) Params(1:np) 
 
 
!  Li(j) = Params(19)
!  wa_0(j)= Params(5)
!  wa_1(j)= Params(6)
!  wa_2(j)= Params(7)
!  wa_3(j)= Params(8)
!end do
!close(14)

     nw_ppf= 4
           a_ppf(1)=1.0d0/(1.0+ 2.5d0) 
           a_ppf(2)=1.0d0/(1.0+ zcut+0.05d0)
           a_ppf(3)=1.0d0/(1.0+ zcut)
           a_ppf(4)=1.0d0/(1.0+ 0.0d0) 
 
!do j=n,1700, -1
 
            w_ppf(1)=0.0 !wa_0(j)
            w_ppf(2)=0.0 !wa_1(j)
            w_ppf(3)=-1.0 !wa_2(j)
            w_ppf(4)=-1.0 !wa_3(j)

!print *,  w_ppf(1),  w_ppf(2),w_ppf(3),w_ppf(4)

do i=0,1500
 za=0.0+0.002*i
 al=1.0/(1+za) 
!if(wa_0(j)>-2.0 .and. wa_0(j) < -0.627 .and. &
!   wa_1(j) > -2.0 .and.  wa_1(j) < 0.0 .and. &
!   wa_2(j) > -0.107951E+01 .and.  wa_2(j) < -0.463209E+0 .and. &
!    wa_3(j) > -0.137057E+01 .and.  wa_3(j) < -0.9855660E+00 )  then 				 

                 print *, za, w_de(al)

!end if
!			enddo
end do

end program nada

