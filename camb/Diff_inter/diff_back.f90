!
!	Diff.f90
!	
!
!	Created by vetovazquez on 11/07/2011.
!	Copyright 2011 __MyCompanyName__. All rights reserved.
!
     program Diff_interpol
implicit none

!Read Files
 character(LEN=*), parameter :: File_1 = 'EOS_wz/wz_neg1.dat'
 character(LEN=*), parameter :: File_2 = 'EOS_wz/wz_neg1.dat'
 
 integer, parameter :: zmax=613, zmax_2=613

 integer            :: i,j 
 real               :: z(zmax), ADD_rs(zmax), z_2(zmax_2), ADD_rs_2(zmax_2)
 real               :: ADV_rs(zmax), ADV_rs_2(zmax_2), H_rs(zmax), H_rs_2(zmax_2)
 real               :: x, z0, ADD_rs0, ADV_rs0, H_rs0
 real               :: Interpol_fun

 integer, parameter :: Nmax =3000
 real, parameter    :: d=2.5/Nmax
 real               :: Int_ADD(Nmax), Int_ADD_2(Nmax)
 real               :: Int_ADV(Nmax), Int_ADV_2(Nmax), Int_H(Nmax), Int_H_2(Nmax)

open(unit=14,file=File_1,form='formatted',status='old')
open(unit=15,file=File_2,form='formatted',status='old')

 do i=1, zmax
   read (14,*) z0, ADD_rs0, ADV_rs0, H_rs0
 
    j=zmax-i+1
    z(j)      = z0
    ADD_rs(j) = ADD_rs0
    ADV_rs(j) = ADV_rs0
    H_rs(j)   = H_rs0
 end do


 do i=1, zmax_2
   read (15,*) z0, ADD_rs0, ADV_rs0, H_rs0

    j=zmax_2-i+1
    z_2(j)      = z0
    ADD_rs_2(j) = ADD_rs0
    ADV_rs_2(j) = ADV_rs0
    H_rs_2(j)   = H_rs0
 end do

close(14)
close(15)



do i=1,Nmax
  x=0.0+d*i

       Int_ADD(i)   = Interpol_fun(x, z,  ADD_rs,  zmax)
       Int_ADD_2(i) = Interpol_fun(x, z_2,ADD_rs_2,zmax_2)

       Int_ADV(i)   = Interpol_fun(x, z,  ADV_rs,  zmax)
       Int_ADV_2(i) = Interpol_fun(x, z_2,ADV_rs_2,zmax_2)

       Int_H(i)     = Interpol_fun(x, z,  H_rs,    zmax)
       Int_H_2(i)   = Interpol_fun(x, z_2,H_rs_2,  zmax_2)
end do


!Printing the Files
do i=1,Nmax
  x=0.0+d*i
  print *, x, Int_ADD_2(i)/Int_ADD(i), Int_ADV_2(i)/Int_ADV(i), Int_H(i)/Int_H_2(i)
end do



!open(unit=11,file=out_name)
! do l=1, lmax
!      write (11,'(1E15.5,1E15.5,1E15.5)') k(l,1),Pk(l,3), Pk(l,4)
! end do
! close(11)
end program Diff_interpol



!Interpolation function
function Interpol_fun(xf, zf, ADD_rsf, zmax)
 IMPLICIT NONE
  integer            :: zmax, j
  real               :: xf, Interpol_fun
  real               :: zf(zmax), ADD_rsf(zmax)
 

if(xf>= zf(1) .or. xf<= zf(zmax)) then
   do  j=1,zmax
       if(xf >= zf(j) .and. xf <= zf(j+1) ) &
       Interpol_fun = (ADD_rsf(j)*(zf(j+1)-xf)+ADD_rsf(j+1)*(xf-zf(j)))/(zf(j+1)-zf(j))
    end do
end if

end function

