
import os, sys, time

opts = """\
\"mode: bycore; \
N: 20; \
threads: 5; \
hostfile: auto; \
job_name: z0.25_norsd_np0.001_nRT10_r02.ini \"
"""
comm = """command:  source ~/.bashrc; \
	command: OMP_NUM_THREADS=%threads% mpirun -hostfile %hostfile% ./cosmomc INI_z0.25_norsd_np0.001_nRT10_r02.ini > chains/Sim/logs/INI_z0.25_norsd_np0.001_nRT10_r02.ini.log 2>chains/Sim/logs/INI_z0.25_norsd_np0.001_nRT10_r02.ini.err"""

print("wq sub -r " + opts + " -c "+comm)

if False:
 for num in range(2,7):
  commd = """
  nohup wq sub  wq_z0.25_norsd_np0.001_nRT10_r0%i.ini & 
  """%(num)
  os.system(commd)
  time.sleep(0.1) 

