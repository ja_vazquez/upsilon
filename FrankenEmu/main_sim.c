/*
 This is a more simplistic version of the main.c file
 */
 
 
#include "stdio.h"
#include "math.h"
#include "stdlib.h"
#include "string.h"

main(int argc, char **argv) {
    int i,j,type=2;
    int t[1];
    double xstar[7], ystar[2*582];
    FILE *fp;
 
    xstar[0]=0.022;
    xstar[1]=0.12;
    xstar[2]=0.96;
    xstar[3]=65.0;
    xstar[4]=-1;
    xstar[5]=0.8;
    xstar[6]=0.5;
    
    t[0]=2;
    emu(xstar, ystar, t);

    printf("Data save in test_pk.dat\n");
    if ((fp = fopen("test_pk3.dat","w"))==NULL) {
        printf("cannot open %s \n","test_pk_f");
        exit(1);
    }
 
    for(j=0; j<582; j++) {
        fprintf(fp ,"%e %e \n", ystar[j], ystar[582+j]);
    }
    fclose(fp);
}
