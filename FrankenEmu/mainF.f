! Fortran file to compute Pk

      program mainF

      integer i, j
      integer tipe  
      integer t(1)
      double precision x(7)
      double precision y(1164)

      !  initialize x to be cosmological parameters 
      !  within the range and redshift
      x(1) = 0.0224   ! omega_b 
      x(2) = 0.12296  ! omega_m
      x(3) = 0.9700   ! n_s
      x(4) = 65.0     ! H0
      x(5) = -1.000   ! w
      x(6) = 0.8000   ! sigma8
      x(7) = 0.5000   ! redshift

      write(*,10), (x(j), j = 1, 7)
10    format (6f7.4 )
  
      ! call the emulator, x: cosmological parameters,
      ! y: vector containing k in first half and P(k) in second half
      ! 1: Delta(k), 2: P(k)

      t(1)=2  
      call emu(x,y,t)

      open(30,file='pk.dat',form='formatted')
     
      do i=1,582
         write(30,*)y(i),y(i+582)
      enddo

      stop
      end

