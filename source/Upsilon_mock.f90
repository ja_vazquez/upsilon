
!Upsilon Module, last update Jun 17

module Upsilon
use cmbdata
use cmbtypes
use constants
use Precision
use likelihood
use Spline
implicit none

type, extends(CosmologyLikelihood) :: UpsLikelihood
   contains
   procedure :: LogLike => Ups_LnLike
end type UpsLikelihood


integer :: use_upsilon
integer :: upsilon_option
logical :: remove_pairs
logical :: virgin=.True.

! Params that describe the files of the mocks
integer :: mock_NP, mock_gg
logical :: use_mock
real :: R0_gg, R0_gm, z_gg, z_gm 
character*100  mock_file
character*100  mock_cov

character*100, dimension(10000) :: lines
character*100 bchi2name
integer nlines


type UpsilonData
   real :: ggR0, gmR0, zdatagg, zdatagm, calibamp, calibcor
   integer :: NP
   logical, allocatable, dimension (:) :: isgg
   real, allocatable, dimension (:) :: rra, dsups
   real(dl), allocatable, dimension (:,:) :: cov, icov

   !! We really use this just to pass parameters around, it changes
   !! and it is not intrinsic part of data
   integer bselector  !! bfuncselector
   real :: bias, biasp, biaspp
end type UpsilonData


type(UpsilonData), target :: DLRG, DDEBUG
type(UpsilonData), pointer :: Dcur
type(CSpline) :: thc_Fid, thc_ns, thc_s8p, thc_s8m, thc_oml,thc_omh,thc_ah,thc_al, thc_h0l, thc_h0h
type(CSpline) :: GalPtA, GalPtB
type(CSpline) :: CoyoSpl

real, parameter :: zdatafid = 0.23, maxrgg=200.0, maxrgm=200.0
!be the actual redshift of simulations. 
real, parameter :: finalcor =0.00
integer, parameter :: MAXNP=50
real*8 :: pi_ = 4*ATAN(1d0)
real*8, parameter :: alnkmin=log(2d-3), alnkmax=log(10d0)
real :: bestchi2=1e30
real :: deltabiasp

    contains

    subroutine UpsLikelihood_Add(LikeList, Ini)
       use IniFile
       use settings
       class(LikelihoodList) :: LikeList
       Type(TIniFile) :: ini
       Type(UpsLikelihood), pointer :: like


       if (Ini_Read_Logical_File(Ini, 'use_Ups',.false.)) then
          allocate(like)
          like%LikelihoodType = 'Ups'
          like%needs_background_functions = .true.
          call LikeList%Add(like)
          if (Feedback>1) write(*,*) 'read Ups datasets'
       end if
    end subroutine UpsLikelihood_Add


    function Ups_LnLike(like, CMB, Theory, DataParams)
       implicit none
       Class(CMBParams) CMB
       Class(UpsLikelihood) :: like
       Class(TheoryPredictions) Theory
       real(mcp) :: DataParams(:)
       real(mcp) Ups_LnLike, UpsilonLike
       real DSXR02 !! the 
       real bias, biasp, biaspp,cross_corr, ds3, rhobar, biase
       real lbias, lbiasp, lbiaspp, mbias, mbiasp, mbiaspp
       integer ii
       real, dimension(MAXNP) :: theo, diff
       real chi2, chi2c,zdata
       real  rl, t1 ,t2, rad

       type (CSpline) :: XiSpl, SigmaGG, SigmaGM
       integer, parameter :: NR = 100 !// from 100 before.
       real, dimension (NR) :: xirr,xival, xival0,xival2
       real :: minr, maxr, corrfact, alphacross, gfactgg, gfactgm
       real :: gfactgg0, gfactgm0, s8, ryr, upscalib
       real*8, external :: rombint
       character*100 tmpstr
       integer nsets, bits
       integer bfuncselector
       character*16 idstr
       real rt
       integer ki, ini_set   
       real*8 kk, h0

       integer t(1) 
       double precision x(6) 
       double precision y(1164)
       real, dimension (582):: new_k, new_pk 

       if (virgin.and.use_upsilon<100) then
          call InitUpsilon
          virgin=.False.
       end if

       print *, 'parameters', CMB%ombh2, CMB%omch2, CMB%InitPower(1)
       print *, CMB%H0, Theory%sigma_8

      !Coyote emulator
      x(1) = CMB%ombh2                  ! omega_b 
      x(2) = CMB%omdmh2 + CMB%ombh2     ! omega_m
      x(3) = CMB%InitPower(1)           ! n_s
      x(4) = -1.000                     ! w
      x(5) = Theory%sigma_8             ! sigma8
      x(6) = zdatafid*1.0               ! redshift
      t(1) = 2                          ! 1= D^2, 2= P(k)        

      if (use_upsilon.eq.128) then
         call emu_noh(x,y,t)
         h0 = CMB%H0/100.0
         do ii=1,582
            new_k(ii) = y(ii)/h0
            new_pk(ii) = y(ii+582)*h0**3.0    
         enddo   
      
         call CoyoSpl%init(new_k, new_pk)

         open (50,file='test_Pk.dat')
           do ki = 1,200
              kk = exp(alnkmin+(ki-1)*(alnkmax-alnkmin)/199.)
              write (50,'(3G)')kk, MatterPowerat_Z(Theory,DBLE(kk),zdatafid*1.0_dl), CoyoSpl%eval(real(kk))
           end do
         close(50)
      end if


       rhobar = 2.77519737e11*(CMB%omdm+CMB%omb+0.0*CMB%omnu)*1e-12

       s8 = Theory%sigma_8
       upscalib = CMB%upscalib

       DLRG%bias = CMB%upsdata(10)/s8
       DLRG%biasp =  CMB%upsdata(11)
       DLRG%biaspp = CMB%upsdata(12)
  
       minr = 2.0 
       maxr = 200.0

       do ii = 1, NR
          rad = minr * (maxr/minr)**(DBLE((ii-1))/DBLE((NR-1)))
          xirr(ii) = rad
          xival0(ii) = Xi(DBLE(rad), CMB,Theory, zdatafid*1.0_dl)
!        print *,'Xi0', xival0(ii)
       end do


       chi2=0
       bits=1
                        ! Here is where the mocks are read
       DCUR => DLRG

       chi2c = GetDataChi2(DCUR)
       chi2=chi2+chi2c
       print *, 'nsets -> ',chi2c

       bits=bits*2

       print *, 'chi2 before calibration', chi2
       chi2 = chi2 + (upscalib**2)/1.**2 !! unit calibration error

       UpsilonLike = (chi2)/2.0  !! no minus is cosmomc.

       !-------------------
       Ups_LnLike=UpsilonLike
       print *, 'Ups_Lnlike',Ups_LnLike
    
    contains

       function GetDataChi2 (D) result (chx2)
          type (UpsilonData) :: D
          real chx2

          bias = D%bias
          biasp = D%biasp
          biaspp = D%biaspp
          bfuncselector = D%bselector

          !Update this function later, for the moment just read out some values
          gfactgg=0.9796553 !MatterPowerat_Z(Theory,0.1_dl,D%zdatagg*1.0_dl)/MatterPowerat_Z(Theory,0.1_dl,zdatafid*1.0_dl)
          gfactgm=0.9796553 !MatterPowerat_Z(Theory,0.1_dl,D%zdatagm*1.0_dl)/MatterPowerat_Z(Theory,0.1_dl,zdatafid*1.0_dl)

          gfactgg0=0.7727202 !MatterPowerat_Z(Theory,0.1_dl,D%zdatagg*1.0_dl)/MatterPowerat_Z(Theory,0.1_dl,0.0_dl)
          gfactgm0=0.7727202 !MatterPowerat_Z(Theory,0.1_dl,D%zdatagm*1.0_dl)/MatterPowerat_Z(Theory,0.1_dl,0.0_dl)
 
          minr = 2.0 
          maxr = 200.0

          do ii=1,NR
             rad = minr * (maxr/minr)**(DBLE((ii-1))/DBLE((NR-1)))
             xirr(ii) = rad
             if (upsilon_option.ne.1.and.upsilon_option.ne.2) then
                xival(ii)=xival0(ii)*XiCorr(CMB,Theory,xirr(ii),(D%zdatagg+D%zdatagm)/2.0)
             else if (upsilon_option.eq.1) then
                xival(ii)=xival0(ii) * XiCorr(CMB,Theory,xirr(ii),zdatafid)
             else if (upsilon_option.eq.2) then
                xival(ii)=xival0(ii) * 1.0
             end if

          end do


          call XiSpl%init(xirr,xival)


          do ii=1, NR
             rad = minr * (maxr/minr)**(DBLE((ii-1))/DBLE((NR-1)))
             xirr(ii) = rad
             xival(ii) = 2*rombint(dsggfunc, 0d0, DBLE(maxrgg), 1d-5)
             xival2(ii) = 2*rombint(dsgmfunc, 0d0, DBLE(maxrgm), 1d-5)
          end do

          call SigmaGG%init(xirr, xival)
          call SigmaGM%init(xirr, xival2)

          do ii=1,D%NP
             if (D%isgg(ii)) then
                theo(ii) = GetUpsilon(SigmaGG,D%rra(ii),D%ggR0)
             else
                theo(ii)=GetUpsilon(SigmaGM,D%rra(ii),D%gmR0)*rhobar*(1.0+upscalib*D%calibamp)*D%calibcor
             end if
          end do


          diff(1:D%NP) = theo(1:D%NP) - D%dsups
          chx2 = DOT_PRODUCT(diff(1:D%NP),MATMUL(D%icov,diff(1:D%NP)))
       end function GetDataChi2


       real*8 function dsggfunc (x)
          real*8 x
          real tr, xi
          real b2
          tr= sqrt(rad**2+x**2)
          xi = XiSpl%eval (tr) * gfactgg

           ! We should fix b_hh to b_hm+0.2
          b2= biasp+deltabiasp
           ! if (tr>3.96 .and.tr<4) write(*,'(10G)') tr, xi, bias, s8, xi*bias**2 
          dsggfunc = xi * bias**2 + 2*bias*b2*gfactgg0**2*GalPtA%eval(tr)*(s8/0.8)**4+ b2**2*gfactgg0**2 * GalPtB%eval(tr)*(s8/0.8)**4
       end function dsggfunc


       real*8 function dsgmfunc(x)
          real*8 x
          real tr, xi

          tr= sqrt(rad**2+x**2)
          xi = XiSpl%eval (tr) * gfactgm
            !!    write (66,*) tr, bias , 1/xi*(biasp * gfactgm0**2 * GalPtA%eval(tr)
            !!      *(s8/0.8)**4) 
            !! gfactgm is the growth factor squared
          dsgmfunc = xi * bias + biasp * gfactgm0**2 * GalPtA%eval(tr)*(s8/0.8)**4
       end function dsgmfunc

    end function Ups_LnLike


    subroutine InitUpsilon
       integer ii, jj, NX
       real :: tmp(1000,11)
       real :: excal

       print *, "Using: zdatafid:",zdatafid, "maxrgg:", maxrgg,"maxrgm:",maxrgm

       call LoadUpsilonData(DLRG, mock_file, mock_cov, mock_NP, mock_gg, R0_gg, R0_gm, z_gg, z_gm, 1.0,0.04, 1, .False.)

        !       call LoadUpsilonData(DLRG,'lrgdata-final/sim_z0.25_norsd_np0.0001.dat',&
        !   'lrgdata-final/joint_sim_z0.25_norsd_np0.0001.dat', 30, 15, 2.0, 2.0, 0.25, 0.25, 1.0,0.04, 1, .False.)


       open (50, file='NLcorrXi_all.dat', status='old')
          read(50,*) NX
          if (NX>1000) stop 'bad shit'
             do ii=1,NX
                read (50,*) tmp(ii,1:11)
             end do
       close(50)

       call thc_Fid%init(tmp(1:NX,1), tmp(1:NX,2))
       call thc_ns%init(tmp(1:NX,1), tmp(1:NX,3))
       call thc_s8p%init(tmp(1:NX,1), tmp(1:NX,4))
       call thc_s8m%init(tmp(1:NX,1), tmp(1:NX,5))
       call thc_oml%init(tmp(1:NX,1), tmp(1:NX,6))
       call thc_omh%init(tmp(1:NX,1), tmp(1:NX,7))

       call thc_ah%init(tmp(1:NX,1), tmp(1:NX,8))
       call thc_al%init(tmp(1:NX,1), tmp(1:NX,9))

       call thc_h0l%init(tmp(1:NX,1), tmp(1:NX,10))
       call thc_h0h%init(tmp(1:NX,1), tmp(1:NX,11))

       call LoadGalPt
    end subroutine InitUpsilon



    subroutine LoadUpsilonData (D, fnamedat, fnamecov, NP, NPGG, ggR0, gmR0,zdatagg, zdatagm, calibcor, calibamp, bs,ZeroOffDiag)
       integer NP, ii, jj, NPGG
       real ggR0, gmR0, calibcor, calibamp
       real zdatagg, zdatagm
       character*(*) fnamedat, fnamecov
       type (UpsilonData) :: D
       logical ZeroOffDiag
       integer bs

       print *,"Loading:", TRIM(fnamedat),',', TRIM(fnamecov)
       allocate (D%isgg(NP), D%rra(NP), D%dsups(NP), D%cov(NP,NP), D%icov(NP,NP))
       D%ggr0 = ggr0
       D%gmr0 = gmr0
       D%np = np
       D%bselector = bs
       D%zdatagg = zdatagg
       D%zdatagm = zdatagm
       D%calibamp = calibamp
       D%calibcor = calibcor
       !! first open data gg data
       open (50, file =fnamedat, status='old')
          do ii=1,NP
             read (50,*) D%rra(ii), D%dsups(ii)
          end do
       close(50)

       open (50, file =fnamecov, status='old')
          do ii=1,NP
             read (50,*) D%cov (ii,1:NP)
          end do
       close(50)

       if (ZeroOffDiag) then
          do ii=1,NP
             do jj=1,NP
               if (Ii.ne.jj) D%cov(ii,jj)=0
             end do
          end do
       end if


       D%isgg(1:NPGG) = .true.
       D%isgg(NPGG+1:NP) = .false.
       D%icov=D%cov
       call Matrix_Inverse(D%icov)


       if (.false.) then 
          print *,'----------------------------:',fnamedat
          print *,'At the end of LoadUpsilonData we have'
          print *, 'calibamp=',D%calibamp, 'calibcor=',D%calibcor
          print *, 'zdatagg=',zdatagg, 'zdatagm=',zdatagm
          print *, 'rad:', D%rra
          print *, 'ups:', D%dsups
          print *, 'COV:', D%cov
       end if
    end subroutine LoadUpsilonData


    subroutine LoadGalPt
       integer, parameter :: NP=1500
       real, DIMENSION(NP) :: r,a,b
       real  tmp
       integer ii

       open (144,file='galpt.dat',status='old')
          do ii=1,NP
             read (144,*) r(ii), tmp, a(ii), b(ii)
          end do
       close(144) 
        !! see Tobias' email
       a=a*2
       b=b*2

       call GalPtA%init(r,a)
       call GalPtB%init(r,b)
    end subroutine LoadGalPt



    real*8 function Xi (ru, CMB, Theory, curz)
       implicit none
       real*8 ru
       real(dl) curz
       Type (CMBParams) CMB
       Type(TheoryPredictions) Theory
       real*8, external :: rombint

       Xi = rombint (xifunc, alnkmin, alnkmax, 1d-5)/(2*pi_**2)
!      print *,'computing XI'
       contains

       real*8 function xifunc (alnk)
          use specfun
          real*8 alnk, ak,x, pow

          ak=exp(alnk)
          x=ak*ru
          pow = MatterPowerat_Z(Theory,DBLE(ak),curz)/(1+finalcor)**2
        print *, 'Pk', ak, pow, CoyoSpl%eval(real(ak))
          xifunc = pow*sin(x)/x * ak**3

!        print *, 'power', ak, pow


       end function xifunc
    end function Xi



    real function XiCorr(CMB,Theory, rad, zz) Result (C)
       implicit none
       Type (CMBParams) CMB
       Type(TheoryPredictions) Theory
       real rad
       real zz, H0, M_nu
       real aa, aafid, nseff
       real, parameter :: H0_fid=70
       if (rad.gt.50) then
          C=1.0
          return
       end if


       H0 = CMB%H0;
       M_nu =CMB%omnuh2*93.14  ! neutrino mass in eV. This is accurate enough for correction
       nseff = CMB%InitPower(1) !!!! we now heave exact derivatives- 0.01*M_nu/0.15
                                        !+(H_0-H_0_fid)/H_0_fid

        !!    print *,'shit happening:',s8boos,
        !(CMB%initpower(1)-1.0),info%theory%sigma_8*sqrt(s8boos)-0.8,(CMB%omb+CMB%omdm)-0.25

       C = 1.0
       C = C +thc_ns%eval(rad)*(nseff-1.0) !! ns
       if (upsilon_option.ne.10) then
          if (Theory%sigma_8.gt.0.8) then
             C = C +thc_s8p%eval(rad)*(Theory%sigma_8-0.8) !! s8
          else
             C = C +thc_s8m%eval(rad)*(Theory%sigma_8-0.8) !! s8
          end if
       end if

       if ((CMB%omb+CMB%omdm).gt.0.25) then
          C = C + thc_omh%eval(rad)*( (CMB%omb+CMB%omdm)-0.25)  !!omega_m
       else
          C = C + thc_oml%eval(rad)*( (CMB%omb+CMB%omdm)-0.25)  !!omega_m
       end if

       if (H0.gt.H0_fid) then
          C = C + thc_h0h%eval(rad)*( H0-H0_fid )  !!H0hi
       else
          C = C + thc_h0l%eval(rad)*( H0-H0_fid )  !!H0lo
       end if

       aa=1./(1.+zz);
       aafid=1/(1.+0.23);
       if (zdatafid.ne.0.23) print *, "WARN:zdatafid"


       if (aa.gt.aafid) then
          C = C + thc_ah%eval(rad) * (aa-aafid)
          !   print *, aa, aafid, rad,(1+thc_ah%eval(rad) * (aa-aafid)),'A'
       else if (aa.lt.aafid) then
       C = C + thc_al%eval(rad) * (aa-aafid)
          !    print *, aa, aafid, rad,(1+thc_al%eval(rad) * (aa-aafid)),'B'
       end if

          !print *, rad, C,'O', (CMB%initpower(1)-1.0), (info%theory%sigma_8-0.8), ((CMB%omb+CMB%omdm)-0.25)

       C = C*thc_Fid%eval(rad)
    end function XiCorr



    function GetUpsilon (Sigma, rad, R0) Result (res)
       implicit none
       type (CSpline) :: Sigma
       real :: rad, res, R0
       real*8, external :: rombint

       res = 2/rad**2* rombint (upsfunc, DBLE(R0), DBLE(rad),1d-5)
       res = res - Sigma%eval(rad) + Sigma%eval(R0)*R0**2/rad**2

       contains
         real*8 function upsfunc(x)
            real*8 x
            upsfunc = Sigma%eval(REAL(x)) * x
         end function upsfunc
    end function GetUpsilon


end module Upsilon
