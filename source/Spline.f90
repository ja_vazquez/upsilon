module Spline


type CSpline
   integer N
   real, dimension (:), allocatable :: x,y,y2

 contains
     procedure :: init => initspline
     procedure :: respline => respline
     procedure :: eval => evalspline
     procedure :: evalarr => evalsplinearr
end type CSpline

contains

  subroutine InitSpline (C,x,y)
    class (CSpline) C
    integer N
    real x(:), y(:)
    

    N= size(x,1)
    C%N=N
!    print *,'we have :',N
    if (allocated(C%x)) deallocate(C%x)
    if (allocated(C%y)) deallocate(C%y)
    if (allocated(C%y2)) deallocate(C%y2)
!    print *,'we have :',N
    allocate (C%x(N), C%y(N), C%y2(N))
    
    C%x(1:N)=x(1:n)
    c%y(1:n)=y(1:n)
    call nrspline (x,y,N,1e30,1e30,C%y2)

  end subroutine InitSpline


  subroutine ReSpline (C)
    class (CSpline) C

    C%y2=0d0
    call nrspline (C%x,C%y,C%N,1e30,1e30,C%y2)

    if (ANY(isnan(C%y2))) stop 'isnan in respline'

  end subroutine ReSpline
  
  function EvalSpline(C,xn) result (res)
    class (CSpline) C
    real xn, res
    call splint (C%x, C%y, C%y2, C%N, xn,res)
  end function EvalSpline

  subroutine EvalSplineArr (C,xar,yar)
    class (CSpline) C
    real, intent(IN) :: xar(:)
    real, intent(OUT) :: yar(:)
    integer :: N,ii
    REAL x,y
    INTEGER k,khi,klo
    REAL a,b,h
    integer nc

!    print *,"EA", C%N
!    print *,'INEA'
    N=size(xar,1)
    nc=1
    klo=1
    khi=2

    do 
       !print *, nc, klo,khi, xar(nc), C%x(klo), C%x(khi)
       if ((xar(nc).ge.C%x(klo)).and.(xar(nc).lt.C%x(khi)).or. &
          ((xar(nc).le.C%x(klo)).and.(klo.eq.1)).or. & 
          ((xar(nc).ge.C%x(khi)).and.(khi.eq.C%N))) then
          x=xar(nc)
          h=C%x(khi)-C%x(klo)
          if (h.eq.0.) pause 'bad C%x input in splint'
          a=(C%x(khi)-x)/h
          b=(x-C%x(klo))/h
          yar(nc)=a*C%y(klo)+b*C%y(khi)+((a**3-a)*C%y2(klo)+(b**3-b)*C%y2(khi))*(h**2)/6.
          nc=nc+1
          if (nc.gt.N) exit
          cycle
       end if
       do while (.not.((xar(nc).ge.C%x(klo)).and.(xar(nc).lt.C%x(khi))))
          klo=klo+1
          khi=khi+1
          if (khi.eq.C%N) exit
       end do
    end do
!    print *,'DONE'
  end subroutine EvalSplineArr


SUBROUTINE nrspline(x,y,n,yp1,ypn,y2)
  INTEGER n,NMAX
  REAL yp1,ypn,x(n),y(n),y2(n)
  PARAMETER (NMAX=50000)
  INTEGER i,k
  REAL p,qn,sig,un,u(n)
  if (yp1.gt..99e30) then
     y2(1)=0.
     u(1)=0.
  else
     y2(1)=-0.5
     u(1)=(3./(x(2)-x(1)))*((y(2)-y(1))/(x(2)-x(1))-yp1)
  endif


  do  i=2,n-1
     sig=(x(i)-x(i-1))/(x(i+1)-x(i-1))
     p=sig*y2(i-1)+2.
     y2(i)=(sig-1.)/p
     u(i)=(6.*((y(i+1)-y(i))/(x(i+1)-x(i))-(y(i)-y(i-1))/(x(i)-x(i-1)))/(x(i+1)-x(i-1))-sig*u(i-1))/p
  end do


  if (ypn.gt..99e30) then
     qn=0.
     un=0.
  else
     qn=0.5
     un=(3./(x(n)-x(n-1)))*(ypn-(y(n)-y(n-1))/(x(n)-x(n-1)))
  endif


  y2(n)=(un-qn*u(n-1))/(qn*y2(n-1)+1.)

  do k=n-1,1,-1
     y2(k)=y2(k)*y2(k+1)+u(k)
  end do


  return
END SUBROUTINE nrspline

SUBROUTINE splint(xa,ya,y2a,n,x,y)
  INTEGER n
  REAL x,y,xa(n),y2a(n),ya(n)
  INTEGER k,khi,klo
  REAL a,b,h
  klo=1
  khi=n
1 if (khi-klo.gt.1) then
     k=(khi+klo)/2
     if(xa(k).gt.x)then
        khi=k
     else
        klo=k
     endif
     goto 1
  endif
  h=xa(khi)-xa(klo)
  if (h.eq.0.) pause 'bad xa input in splint'
  a=(xa(khi)-x)/h
  b=(x-xa(klo))/h
  y=a*ya(klo)+b*ya(khi)+((a**3-a)*y2a(klo)+(b**3-b)*y2a(khi))*(h**2)/6.
  return
END SUBROUTINE splint


  
end module Spline
