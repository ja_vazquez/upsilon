

    module Upsilon
    use cmbdata
    use cmbtypes
    use constants
    use Precision
    use likelihood
    use Spline
    implicit none

    type, extends(CosmologyLikelihood) :: UpsLikelihood
    contains
    procedure :: LogLike => Ups_LnLike
    end type UpsLikelihood



integer :: use_upsilon
integer :: upsilon_option
logical :: remove_pairs
logical :: virgin=.True.
logical :: use_mock

character*100, dimension(10000) :: lines
character*100 bchi2name
integer nlines

type UpsilonData
   real :: ggR0, gmR0, zdatagg, zdatagm, calibamp, calibcor
   integer :: NP
   logical, allocatable, dimension (:) :: isgg
   real, allocatable, dimension (:) :: rra, dsups
   real(dl), allocatable, dimension (:,:) :: cov, icov

   !! We really use this just to pass parameters around, it changes
   !! and it is not intrinsic part of data
   integer bselector  !! bfuncselector
   real :: bias, biasp, biaspp

end type UpsilonData

type(UpsilonData), target :: DLRG, DLRG_HZ , DMAINL3, DMAINL4, DMAINL5, DDEBUG
type(UpsilonData), pointer :: Dcur

real, parameter :: zdatafid = 0.23, maxrgg=250.0, maxrgm=250.0
!real, parameter :: zdatafid = 0.23, maxrgg=60.0, maxrgm=140.0  !! fiducal must
!be the actual redshift of simulations. 
!real, parameter :: zdatafid = 0.23, maxrgg=50.0, maxrgm=50.0  !! fiducal must
!be the actual redshift of simulations. 
real, parameter :: finalcor =0.00

integer, parameter :: MAXNP=50

real*8 :: pi_ = 4*ATAN(1d0)
real*8, parameter :: alnkmin=log(1d-3), alnkmax=log(30d0)

real :: bestchi2=1e30

real :: deltabiasp

type(CSpline) :: thc_Fid, thc_ns, thc_s8p, thc_s8m, thc_oml, thc_omh,thc_ah,thc_al, thc_h0l, thc_h0h

type(CSpline) :: GalPtA, GalPtB



    contains

    subroutine UpsLikelihood_Add(LikeList, Ini)
    use IniFile
    use settings
    class(LikelihoodList) :: LikeList
    Type(TIniFile) :: ini
    Type(UpsLikelihood), pointer :: like


    if (Ini_Read_Logical_File(Ini, 'use_Ups',.false.)) then
            allocate(like)
            like%LikelihoodType = 'Ups'
            like%needs_background_functions = .true.
            call LikeList%Add(like)
        if (Feedback>1) write(*,*) 'read Ups datasets'
    end if

    end subroutine UpsLikelihood_Add


    function Ups_LnLike(like, CMB, Theory, DataParams)
implicit none
    Class(CMBParams) CMB
    Class(UpsLikelihood) :: like
    Class(TheoryPredictions) Theory
    real(mcp) :: DataParams(:)
    real(mcp) Ups_LnLike, UpsilonLike

!---------------


    real DSXR02 !! the 
    real bias, biasp, biaspp,cross_corr, ds3, rhobar, biase
    real lbias, lbiasp, lbiaspp, mbias, mbiasp, mbiaspp
    integer ii
    real, dimension(MAXNP) :: theo, diff
    real chi2, chi2c,zdata

    real  rl, t1 ,t2, rad

    type (CSpline) :: XiSpl, SigmaGG, SigmaGM
    integer, parameter :: NR = 100 !// from 100 before.
    real, dimension (NR) :: xirr,xival, xival0,xival2
    real :: minr, maxr, corrfact, alphacross, gfactgg, gfactgm
    real :: gfactgg0, gfactgm0, s8, ryr, upscalib
    real*8, external :: rombint
    character*100 tmpstr
    integer nsets, bits
    integer bfuncselector
    character*16 idstr
    real rt
    integer ki, ini_set   
    real*8 kk




if (virgin.and.use_upsilon<100) then
       call InitUpsilon
       virgin=.False.
    end if


    write (IDSTR,*) use_upsilon
    IDSTR=ADJUSTL(IDSTR)

    rhobar = 2.77519737e11*(CMB%omdm+CMB%omb+CMB%omnu)*1e-12


    if (use_upsilon.eq.606) then
       print *,'s8=',Theory%sigma_8, cmb%omb, cmb%omdm, cmb%omb+cmb%omdm, cmb%H0

       print *,'g^2=',MatterPowerat_Z(Theory,0.1_dl,zdatafid*1.0_dl)/MatterPowerat_Z(Theory,0.1_dl,0.0_dl)

       stop
    end if



   if (use_upsilon.eq.607) then
       print *,'s8=',Theory%sigma_8, cmb%omb, cmb%omdm, cmb%omb+cmb%omdm, cmb%H0


       open (50,file='Pk.dat')
       do ki = 1,200
          kk = exp(alnkmin+(ki-1)*(alnkmax-alnkmin)/199.)
          write (50,'(3G)') kk,MatterPowerat_Z(Theory,DBLE(kk),0.0_dl),MatterPowerat_Z(Theory,DBLE(kk),zdatafid*1.0_dl)
       end do
       close(50)

       open (50,file='Xi.dat')
       minr=1.
       maxr=250.
       do ii=1,NR
          rad = minr * (maxr/minr)**(DBLE((ii-1))/DBLE((NR-1)))
          write (50,'(4G)') rad, Xi(DBLE(rad), CMB,Theory, 0.0_dl),Xi(DBLE(rad), CMB,Theory, zdatafid*1.0_dl)
       end do
       close(50)


       stop
    end if



   s8 = Theory%sigma_8

    upscalib = CMB%upscalib
!print *, 'Ups_data parameters'
!print *, CMB%upsdata(1), CMB%upsdata(2), CMB%upsdata(3), CMB%upsdata(4)
!print *, CMB%upsdata(5), CMB%upsdata(6), CMB%upsdata(7), CMB%upsdata(8)
!print *, CMB%upsdata(9), CMB%upsdata(10), CMB%upsdata(11), CMB%upsdata(12)
!print *,'Ups params', s8, CMB%upsdata(14), CMB%upsdata(15)
!print *, 'parameters', CMB%H0, CMB%omdm+CMB%omb

    DMAINL3%bias = CMB%upsdata(1)/s8
    DMAINL3%biasp =  CMB%upsdata(2)
    DMAINL3%biaspp = CMB%upsdata(3)

    DMAINL4%bias = CMB%upsdata(4)/s8
    DMAINL4%biasp =  CMB%upsdata(5)
    DMAINL4%biaspp = CMB%upsdata(6)

    DMAINL5%bias = CMB%upsdata(7)/s8
    DMAINL5%biasp =  CMB%upsdata(8)
    DMAINL5%biaspp = CMB%upsdata(9)

    DLRG%bias = CMB%upsdata(10)/s8
    DLRG%biasp =  CMB%upsdata(11)
    DLRG%biaspp = CMB%upsdata(12)

    DLRG_HZ%bias = CMB%upsdata(13)/s8
    DLRG_HZ%biasp =  CMB%upsdata(14)
    DLRG_HZ%biaspp = CMB%upsdata(15)


!!! debug follows DLRG
    DDEBUG%bias = CMB%upsdata(10)/s8
    DDEBUG%biasp =  CMB%upsdata(11)
    DDEBUG%biaspp = CMB%upsdata(12)


    minr = 2.0
    maxr = 250.0 !100*sqrt(2.0)

    nlines=0


    do ii=1,NR
       rad = minr * (maxr/minr)**(DBLE((ii-1))/DBLE((NR-1)))
       xirr(ii) = rad
       xival0(ii) = Xi(DBLE(rad), CMB,Theory, zdatafid*1.0_dl)
       nlines=nlines+1
       write (64,*) xirr(ii), xival0(ii)
       write (lines(nlines),'(4G)') xirr(ii), xival0(ii)!,s8
    end do



    nlines=nlines+1
    lines(nlines) = "-------------"


    chi2=0
    bits=1

    if(use_mock) then
         ini_set = 6
    else
         ini_set = 1
    endif 

!    do nsets=1,6
!       print *, 'nsets', nsets 
!       if (IAND(use_upsilon,bits).gt.0) then
!print *, 'hiiiiii_111'
!          select case(nsets)
!          case (1)  ! 1
!             DCUR => DMainL3
!             stop "Can't do L3"
!          case (2)  ! 2
!             DCUR => DMainL4
!             stop "Can't do L3"
!          case (3)  ! 4
!             DCUR => DMainL5
!          case (4)  ! 8
             DCUR => DLRG
!          case (5)  ! 16
!             DCUR => DLRG_HZ
!          case (6)  ! 32
!print *, 'hiiiiii_333'
!             DCUR => DDEBUG
!          end select
!print *, '--Did something?'        
          chi2c = GetDataChi2(DCUR)
          chi2=chi2+chi2c
          print *, 'nsets -> ',chi2c
!       end if

       bits=bits*2
!    end do

print *, 'chi2 before calibration', chi2
    chi2 = chi2 + (upscalib**2)/1.**2 !! unit calibration error

    UpsilonLike = (chi2)/2.0  !! no minus is cosmomc.

!    print *,'s8=',s8, chi2,2*36+32

!     if (UpsilonLike.lt.bestchi2) then
!        bestchi2=UpsilonLike
!        print *,'!!!!!!!!!!!!!!!!HEY OUT!!!!!!'
!
!           open (14,file='test.bchi2', status='replace')
!           write (14,'(10G)') s8, (CMB%omb+CMB%omdm),DMAINL5%bias,DMAINL5%biasp, DLRG%bias, DLRG%biasp, DLRG_HZ%bias, DLRG_HZ%biasp,upscalib,chi2
!           do ii=1,nlines
!              write (14,'(99A)') lines(ii)
!           end do
!           close(14)
!     end if


!print *, 'finaaaaaal', UpsilonLike
!-------------------

    Ups_LnLike=UpsilonLike
print *, 'Ups_Lnlike',Ups_LnLike
    

contains

      function GetDataChi2 (D) result (chx2)
        type (UpsilonData) :: D
        real chx2

        bias = D%bias
        biasp = D%biasp
        biaspp = D%biaspp
        bfuncselector = D%bselector



        gfactgg=MatterPowerat_Z(Theory,0.1_dl,D%zdatagg*1.0_dl)/MatterPowerat_Z(Theory,0.1_dl,zdatafid*1.0_dl)
        gfactgm=MatterPowerat_Z(Theory,0.1_dl,D%zdatagm*1.0_dl)/MatterPowerat_Z(Theory,0.1_dl,zdatafid*1.0_dl)

        gfactgg0=MatterPowerat_Z(Theory,0.1_dl,D%zdatagg*1.0_dl)/MatterPowerat_Z(Theory,0.1_dl,0.0_dl)
        gfactgm0=MatterPowerat_Z(Theory,0.1_dl,D%zdatagm*1.0_dl)/MatterPowerat_Z(Theory,0.1_dl,0.0_dl)

!!!!!        print *, gfactgg,gfactgm


        minr = 2.0
        maxr = 250.0 !100*sqrt(2.0)
!!!!!        print *,'Upsilon option=', upsilon_option
        do ii=1,NR
           rad = minr * (maxr/minr)**(DBLE((ii-1))/DBLE((NR-1)))
           xirr(ii) = rad
           if (upsilon_option.ne.1.and.upsilon_option.ne.2) then
              xival(ii)=xival0(ii)*XiCorr(CMB,Theory,xirr(ii),(D%zdatagg+D%zdatagm)/2.0)
              !write (64,*) rad, xival0(ii) * gfactgg, xival0(ii) *
              !gfactgg*XiCorr(CMB,Info,xirr(ii),D%zdatagg)
           else if (upsilon_option.eq.1) then
              xival(ii)=xival0(ii) * XiCorr(CMB,Theory,xirr(ii),zdatafid)
           else if (upsilon_option.eq.2) then
              xival(ii)=xival0(ii) * 1.0
           end if

        end do
        call XiSpl%init(xirr,xival)


        minr = 2.0 !min(D%ggR0, D%gmR0)
        maxr = 250.0 !70


       do ii=1, NR
           rad = minr * (maxr/minr)**(DBLE((ii-1))/DBLE((NR-1)))
           xirr(ii) = rad
           xival(ii) = 2*rombint(dsggfunc, 0d0, DBLE(maxrgg), 1d-5)
           xival2(ii) = 2*rombint(dsgmfunc, 0d0, DBLE(maxrgm), 1d-5)

           nlines=nlines+1
           write (lines(nlines),'(4G)') xirr(ii), xival(ii), xival2(ii)

        end do



        nlines=nlines+1
        lines(nlines)="---------------------------------"


        call SigmaGG%init(xirr, xival)
        call SigmaGM%init(xirr, xival2)

       do ii=1,D%NP

           if (D%isgg(ii)) then
              theo(ii) = GetUpsilon(SigmaGG,D%rra(ii),D%ggR0)
           else
              theo(ii)=GetUpsilon(SigmaGM,D%rra(ii),D%gmR0)*rhobar*(1.0+upscalib*D%calibamp)*D%calibcor
           end if

           nlines=nlines+1
           write (lines(nlines),'(4G)')D%rra(ii),D%dsups(ii),sqrt(D%cov(ii,ii)), theo(ii)

        end do


        diff(1:D%NP) = theo(1:D%NP) - D%dsups
        chx2 = DOT_PRODUCT(diff(1:D%NP),MATMUL(D%icov,diff(1:D%NP)))
      end function GetDataChi2


      real*8 function dsggfunc (x)
        real*8 x
        real tr, xi
        real b2
        tr= sqrt(rad**2+x**2)
        xi = XiSpl%eval (tr) * gfactgg

        !! We should fix b_hh to b_hm+0.2

        b2= biasp+deltabiasp
!!!!        if (tr>3.96 .and.tr<4) write(*,'(10G)') tr, xi, bias, s8, xi*bias**2 
        dsggfunc = xi * bias**2 + 2*bias*b2*gfactgg0**2*GalPtA%eval(tr)*(s8/0.8)**4+ b2**2*gfactgg0**2 * GalPtB%eval(tr)*(s8/0.8)**4
      end function dsggfunc

      real*8 function dsgmfunc(x)
        real*8 x
        real tr, xi

        tr= sqrt(rad**2+x**2)
        xi = XiSpl%eval (tr) * gfactgm
!!!!        write (66,*) tr, bias , 1/xi*(biasp * gfactgm0**2 * GalPtA%eval(tr)
!!!!        *(s8/0.8)**4) 
        !! gfactgm is the growth factor squared
        dsgmfunc = xi * bias + biasp * gfactgm0**2 * GalPtA%eval(tr)*(s8/0.8)**4

      end function dsgmfunc



    end function Ups_LnLike



 subroutine InitUpsilon
    integer ii, jj, NX
    real :: tmp(1000,11)
    real :: excal

    print *, "Using: zdatafid:",zdatafid, "maxrgg:", maxrgg,"maxrgm:",maxrgm

!!! Calibration from Rachel's README - not anymore

    if (upsilon_option.eq.30) then
       deltabiasp=0.2
    else
       deltabiasp=0.0
    end if

    if (upsilon_option.eq.40) then
       excal=0.26
    else
       excal=0.0
    end if

!print *,'-------', use_mock, .not. use_mock
if (.not. use_mock) then
!print *, '---Do something'
!!!!    call LoadUpsilonData
!!!!    (DMainL5,'lrgdata-final/jointdata.upsgg2.upsgm2.mainL5.calib.out', &
!!!    'lrgdata-final/jointcov.upsgg2.upsgm2.mainL5.calib.txt', 28, 14, 2.0, 2.0,
!!!    0.115, 0.109, 1.0,0.04+excal, 1, upsilon_option.eq.20)

    call LoadUpsilonData(DMainL5,'lrgdata-final/jointdata.upsgg4.upsgm2.mainL5.calib.out',&
   'lrgdata-final/jointcov.upsgg4.upsgm2.mainL5.calib.txt', 24, 10, 4.0, 2.0, 0.115, 0.109, 1.0,0.04+excal, 1, upsilon_option.eq.20)
  
    if (upsilon_option.ge.1000) then
       remove_pairs=.true.
       upsilon_option = upsilon_option - 1000
    else
       remove_pairs=.false.
    end if



    if (.not.remove_pairs) then

       if (upsilon_option.ge.80.and.upsilon_option.lt.99) then
          if (upsilon_option.eq.92) call LoadUpsilonData(DLRG,'lrgdata-final/simdata.simR2.out', &
    'lrgdata-final/simdata.simR2.cov', 36, 18, 2.0, 2.0, 0.23, 0.23, 1.0,0.04,1, .False.)
          if (upsilon_option.eq.93) call LoadUpsilonData(DLRG,'lrgdata-final/simdata.simR3.out', &
    'lrgdata-final/simdata.simR3.cov', 36, 18, 3.0, 2.0, 0.23, 0.23, 1.0,0.04, 1, .False.)
          if (upsilon_option.eq.94) call LoadUpsilonData(DLRG,'lrgdata-final/simdata.simR4.out', &
    'lrgdata-final/simdata.simR4.cov', 36, 18, 4.0, 2.0, 0.23, 0.23, 1.0,0.04, 1, .False.)
          if (upsilon_option.eq.95) call LoadUpsilonData(DLRG,'lrgdata-final/simdata.simR5.out', &
    'lrgdata-final/simdata.simR5.cov', 36, 18, 5.0, 2.0, 0.23, 0.23, 1.0,0.04, 1, .False.)
          if (upsilon_option.eq.96) call LoadUpsilonData(DLRG,'lrgdata-final/simdata.simR6.out', &
    'lrgdata-final/simdata.simR6.cov', 36, 18, 6.0, 2.0, 0.23, 0.23, 1.0,0.04, 1, .False.)

          if (upsilon_option.eq.80) call LoadUpsilonData(DLRG,'lrgdata-final/sim2data.out', &
    'lrgdata-final/jointcov.upsgg4.upsgm2.LRG-highz.calib.txt', 32, 14, 4.0, 2.0, 0.23, 0.23, 1.0,0.04, 1, .False.)

          if (upsilon_option.eq.82) call LoadUpsilonData(DLRG,'lrgdata-final/sim12data.out', &
      'lrgdata-final/jointcov.upsgg2.upsgm2.LRG.calib.txt', 36, 18, 2.0, 2.0, 0.23, 0.23, 1.0,0.04, 1, .False.)

          if (upsilon_option.eq.84) call LoadUpsilonData(DLRG,'lrgdata-final/sim14data.out', &
      'lrgdata-final/jointcov.upsgg4.upsgm2.LRG.calib.txt', 32, 14, 4.0, 2.0, 0.23, 0.23, 1.0,0.04, 1, .False.)

          if (upsilon_option.eq.86) call LoadUpsilonData(DLRG,'lrgdata-final/sim16data.out', &
               'lrgdata-final/jointcov.upsgg6.upsgm2.LRG.calib.txt',  30, 12, 6.0, 2.0, 0.23, 0.23, 1.0,0.04, 1, .False.)


       else


!!!!!          call LoadUpsilonData
!!!!(DLRG,'lrgdata-final/jointdata.upsgg4.upsgm2.LRG.calib.out', &
!!!!!               'lrgdata-final/jointcov.upsgg4.upsgm2.LRG.calib.txt', 32, 14,
!!!!4.0, 2.0, 0.286, 0.257, 1.0,0.04+excal, 1, upsilon_option.eq.20)

          call LoadUpsilonData(DLRG,'lrgdata-final/jointdata.upsgg4.upsgm2.LRG.lenswt.calib.out',&
      'lrgdata-final/jointcov.upsgg4.upsgm2.LRG.lenswt.calib.txt', 32, 14, 4.0, 2.0, 0.286, 0.257, 1.0,0.04+excal, 1, upsilon_option.eq.20)


       end if

       call LoadUpsilonData(DLRG_HZ,'lrgdata-final/jointdata.upsgg4.upsgm2.LRG-highz.lenswt.calib.out', &
     'lrgdata-final/jointcov.upsgg4.upsgm2.LRG-highz.lenswt.calib.txt', 32, 14, 4.0,2.0, 0.4, 0.4, 1.0,0.05+excal, 1, upsilon_option.eq.20)
 
    else
       print *, "loading remove pairs data"

       call LoadUpsilonData(DLRG,'lrgdata-final/jointdata.upsgg2.upsgm2.LRG.removepairs.calib.out', &
   'lrgdata-final/jointcov.upsgg2.upsgm2.LRG.removepairs.calib.txt', 36, 18, 2.0, 2.0, 0.286, 0.257, 1.0,0.04, 1, .False.)
 
       call LoadUpsilonData(DLRG_HZ,'lrgdata-final/jointdata.upsgg2.upsgm2.LRG-highz.removepairs.calib.out', &
'lrgdata-final/jointcov.upsgg2.upsgm2.LRG-highz.removepairs.calib.txt', 36, 18, 2.0, 2.0,0.4, 0.4, 1.0,0.05, 1, .False.)
 
    end if

!!!!    call LoadUpsilonData (DDEBUG, 'debugdata/mean.gg.gm.out', &
!!!!o    'debugdata/cov.gg.gm.out', 18, 18, 2.0,2.0, 0.5, 0.5, 1., 1., 1, .False.)

else
!print *, '---Do something_2'
!print *, '***using mocks'
!       call LoadUpsilonData(DLRG,'lrgdata-final/jointdata.upsgg2.upsgm2.LRG.removepairs.calib.out',&
!   'lrgdata-final/jointcov.upsgg2.upsgm2.LRG.removepairs.calib.txt', 36, 18, 2.0, 2.0, 0.286, 0.257, 1.0,0.04, 1, .False.)

           !call LoadUpsilonData (DDEBUG, 'debugdata/mean.gg.gm.out', &
           !  'debugdata/cov.gg.gm.out', 18, 18, 2.0,2.0, 0.5, 0.5, 1., 1., 1,.False.)
       call LoadUpsilonData(DLRG,'lrgdata-final/sim_z0.25_norsd_np0.0001.dat',&
   'lrgdata-final/joint_sim_z0.25_norsd_np0.0001.dat', 30, 15, 2.0, 2.0, 0.25, 0.25, 1.0,0.04, 1, .False.)
endif

    open (50, file='NLcorrXi_all.dat', status='old')

    read(50,*) NX
    if (NX>1000) stop 'bad shit'
    do ii=1,NX
       read (50,*) tmp(ii,1:11)
    end do

    call thc_Fid%init(tmp(1:NX,1), tmp(1:NX,2))
    call thc_ns%init(tmp(1:NX,1), tmp(1:NX,3))
    call thc_s8p%init(tmp(1:NX,1), tmp(1:NX,4))
    call thc_s8m%init(tmp(1:NX,1), tmp(1:NX,5))
    call thc_oml%init(tmp(1:NX,1), tmp(1:NX,6))
    call thc_omh%init(tmp(1:NX,1), tmp(1:NX,7))

    call thc_ah%init(tmp(1:NX,1), tmp(1:NX,8))
    call thc_al%init(tmp(1:NX,1), tmp(1:NX,9))

    call thc_h0l%init(tmp(1:NX,1), tmp(1:NX,10))
    call thc_h0h%init(tmp(1:NX,1), tmp(1:NX,11))

    call LoadGalPt


  end subroutine InitUpsilon



  subroutine LoadUpsilonData (D, fnamedat, fnamecov, NP, NPGG, ggR0, gmR0,zdatagg, zdatagm, calibcor, calibamp, bs,ZeroOffDiag)
    integer NP, ii, jj, NPGG
    real ggR0, gmR0, calibcor, calibamp
    real zdatagg, zdatagm
    character*(*) fnamedat, fnamecov
    type (UpsilonData) :: D
    logical ZeroOffDiag
    integer bs

    print *,"Loading:", TRIM(fnamedat),',', TRIM(fnamecov)
    allocate (D%isgg(NP), D%rra(NP), D%dsups(NP), D%cov(NP,NP), D%icov(NP,NP))
    D%ggr0 = ggr0
    D%gmr0 = gmr0
    D%np = np
    D%bselector = bs
    D%zdatagg = zdatagg
    D%zdatagm = zdatagm
    D%calibamp = calibamp
    D%calibcor = calibcor
    !!! first open data gg data
    open (50, file =fnamedat, status='old')
    do ii=1,NP
       read (50,*) D%rra(ii), D%dsups(ii)
    end do
    close(50)

    open (50, file =fnamecov, status='old')

    do ii=1,NP
       read (50,*) D%cov (ii,1:NP)
    end do
    close(50)


    if (ZeroOffDiag) then
       do ii=1,NP
          do jj=1,NP
             if (Ii.ne.jj) D%cov(ii,jj)=0
          end do
       end do

    end if



    D%isgg(1:NPGG) = .true.
    D%isgg(NPGG+1:NP) = .false.

    if (upsilon_option.eq.10) then
       do ii=NPGG+1, NP
          D%cov(ii,ii) = 1e30
       end do
    end if

    if (upsilon_option.ne.13) then
       print *, 'BY DEFAULT WE REMOVE THE FIRST POINT IN GM!!!!!'
       print *, ' YOU HAVE BEEN WARNED.'
       D%cov(:,NPGG+1) = 0.0
       D%cov(NPGG+1,:) = 0.0
       D%cov(NPGG+1,NPGG+1) = 1e30
    end if


!!! We now do this in the main part of the code...
!!!    !Apply calibration corretion
!!!!    D%cov (NPGG+1:NP, NPGG+1:NP) = D%cov (NPGG+1:NP, NPGG+1:NP) /(calibcor**2)
!!!!    D%cov (1:NPGG, NPGG+1:NP) = D%cov (1:NPGG, NPGG+1:NP) /(calibcor)
!!!!    D%cov (NPGG+1:NP, 1:NPGG) = D%cov (NPGG+1:NP, 1:NPGG) /(calibcor)
!!!!    D%dsups (NPGG+1:NP) =D%dsups (NPGG+1:NP)/(calibcor)

    D%icov=D%cov
    call Matrix_Inverse(D%icov)

    !!! crazy correction, not neede anymore
    !!!D%icov = D%icov * (100.-(D%NP)-2.)/(100.-1.)


    if (.false.) then
    !!!!! 
       print *,'----------------------------:',fnamedat
       print *,'At the end of LoadUpsilonData we have'
       print *, 'calibamp=',D%calibamp, 'calibcor=',D%calibcor
       print *, 'zdatagg=',zdatagg, 'zdatagm=',zdatagm
       print *, 'rad:', D%rra
       print *, 'ups:', D%dsups
       print *, 'COV:', D%cov
    end if
  end subroutine LoadUpsilonData


  subroutine LoadGalPt
    integer, parameter :: NP=1500
    real, DIMENSION(NP) :: r,a,b
    real  tmp
    integer ii

    open (144,file='galpt.dat',status='old')
    do ii=1,NP
       read (144,*) r(ii), tmp, a(ii), b(ii)
    end do

!! see Tobias' email
    a=a*2
    b=b*2

    call GalPtA%init(r,a)
    call GalPtB%init(r,b)

  end subroutine LoadGalPt



real*8 function Xi (ru, CMB, Theory, curz)
implicit none
real*8 ru
real(dl) curz
Type (CMBParams) CMB
Type(TheoryPredictions) Theory
!Type(ParamSetInfo) Info
real*8, external :: rombint

  Xi = rombint (xifunc, alnkmin, alnkmax, 1d-5)/(2*pi_**2)

contains

    real*8 function xifunc (alnk)
      use specfun
      real*8 alnk, ak,x, pow

      ak=exp(alnk)
      x=ak*ru
      pow = MatterPowerat_Z(Theory,DBLE(ak),curz)/(1+finalcor)**2
      xifunc = pow*sin(x)/x * ak**3
!print *,'veto' ,ak
    end function xifunc
end function Xi



  real function XiCorr(CMB,Theory, rad, zz) Result (C)
implicit none
    Type (CMBParams) CMB
!    Type(ParamSetInfo) Info
Type(TheoryPredictions) Theory
    real rad
    real zz, H0, M_nu
    real aa, aafid, nseff
    real, parameter :: H0_fid=70
    if (rad.gt.50) then
       C=1.0
       return
    end if

!Theory%sigma_8= CMB%hola !0.8d0
!CMB%omb =  0.04d0
!CMB%omdm = 0.21d0
!cmb%H0 = 70.0d0


    H0 = CMB%H0;
    M_nu =CMB%omnuh2*93.14  ! neutrino mass in eV. This is accurate enough for
!forrection
    nseff = CMB%InitPower(1) !!!! we now heave exact derivatives- 0.01*M_nu/0.15
!+(H_0-H_0_fid)/H_0_fid

!    print *, nseff, M_nu, H_0

!!    print *,'shit happening:',s8boos,
!(CMB%initpower(1)-1.0),info%theory%sigma_8*sqrt(s8boos)-0.8,
!(CMB%omb+CMB%omdm)-0.25

!    print *,'Cin=',C
    C = 1.0
    C = C +thc_ns%eval(rad)*(nseff-1.0) !! ns
    if (upsilon_option.ne.10) then
       if (Theory%sigma_8.gt.0.8) then
          C = C +thc_s8p%eval(rad)*(Theory%sigma_8-0.8) !! s8
       else
          C = C +thc_s8m%eval(rad)*(Theory%sigma_8-0.8) !! s8
       end if
    end if
    if ((CMB%omb+CMB%omdm).gt.0.25) then
       C = C + thc_omh%eval(rad)*( (CMB%omb+CMB%omdm)-0.25)  !!omega_m
    else
       C = C + thc_oml%eval(rad)*( (CMB%omb+CMB%omdm)-0.25)  !!omega_m
    end if

    if (H0.gt.H0_fid) then
       C = C + thc_h0h%eval(rad)*( H0-H0_fid )  !!H0hi
    else
       C = C + thc_h0l%eval(rad)*( H0-H0_fid )  !!H0lo
    end if

    aa=1./(1.+zz);
    aafid=1/(1.+0.23);
    if (zdatafid.ne.0.23) print *, "WARN:zdatafid"


    if (aa.gt.aafid) then
       C = C + thc_ah%eval(rad) * (aa-aafid)
!       print *, aa, aafid, rad,(1+thc_ah%eval(rad) * (aa-aafid)),'A'

    else if (aa.lt.aafid) then
       C = C + thc_al%eval(rad) * (aa-aafid)
!       print *, aa, aafid, rad,(1+thc_al%eval(rad) * (aa-aafid)),'B'
    end if

!    print *, rad, C,'O', (CMB%initpower(1)-1.0), (info%theory%sigma_8-0.8), (
!    (CMB%omb+CMB%omdm)-0.25)
!    print *, CMB%norm
!    print *,'Cout=',C

    C = C*thc_Fid%eval(rad)
  end function XiCorr




      function GetUpsilon (Sigma, rad, R0) Result (res)
implicit none
        type (CSpline) :: Sigma
        real :: rad, res, R0
        real*8, external :: rombint

        res = 2/rad**2* rombint (upsfunc, DBLE(R0), DBLE(rad),1d-5)
        res = res - Sigma%eval(rad) + Sigma%eval(R0)*R0**2/rad**2

      contains
        real*8 function upsfunc(x)
          real*8 x
          upsfunc = Sigma%eval(REAL(x)) * x
        end function upsfunc
      end function GetUpsilon




end module Upsilon
