import os, sys, time


lnum = 2, 3, 4, 5, 6
lnp = 30, 26, 24, 24, 22

for n in range(0,5):

  num = lnum[n] 
  np  = lnp[n]

  ini_input = """
#DEFAULT(batch1/CAMspec_defaults.ini)
#DEFAULT(batch1/lowl.ini)
#DEFAULT(batch1/lowLike.ini)
 
#planck lensing
#DEFAULT(batch1/lensing.ini)
#INCLUDE(batch1/BAO.ini)
#INCLUDE(batch1/HST.ini)
#INCLUDE(batch1/WMAP.ini)

#general settings
DEFAULT(batch1/common_batch1.ini)

#high for new runs
MPI_Max_R_ProposeUpdate = 30

propose_matrix=
#planck_covmats/base_planck_lowl_lowLike.covmat

start_at_bestfit =F
feedback=0
use_fast_slow = F

#sampling_method=7 is a new fast-slow scheme good for Planck
sampling_method = 1
dragging_steps  = 5
propose_scale = 2

indep_sample=0

use_clik= F
action = 0

#these are just small speedups for testing
get_sigma8=T

#Uncomment this if you don't want 0.06eV neutrino by default
#num_massive_neutrinos=3
#param[mnu] = 0 0 0 0 0

#-------------------------------------##


use_Ups = T
use_mock = T
 
file_root=chains/Sim/sim_z0.25_norsd_np0.001_nRT10_r0%i_ups

mock_file = lrgdata-final/mocks_lrg/new_sim_z0.25_norsd_np0.001_nRT10_r0%i_ups.dat
mock_cov =  lrgdata-final/mocks_lrg/new_sim_z0.25_norsd_np0.001_nRT10_r0%i_cov.dat

R0_gg = %i.0
R0_gm = %i.0
z_gg  = 0.25
z_gm  = 0.25
mock_NP = %i
mock_gg = %i

  """%(num, num, num, num, num, np, np//2)



  with open('INI_z0.25_norsd_np0.001_nRT10_r0%i.ini'%(num), 'w') as f:
       f.write(ini_input)	
     

